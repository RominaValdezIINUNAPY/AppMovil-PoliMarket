
import {ProductosPage} from "../productos/productos";
import {UsernameService} from "../../services/user.service";
import {Component, Injectable,OnInit} from '@angular/core';
import { NavController , NavParams, IonicPage} from 'ionic-angular';
import {CarritoService} from "../../services/carrito.service";
import {Producto} from "../../models/producto";
import {LoginPage} from "../login/login";
//import {ProductosService} from "./productos.service";
import {Http} from "@angular/http";

import {AngularFireDatabase} from 'angularfire2/database';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'page-mi-carrito',
  templateUrl: 'mi-carrito.html'
})

export class MiCarritoPage {
  usuario = "Nombre del usuario";
  compras:any[] = [];
  productos_filtrado:any[] = [];
  cargando:boolean = true;

  constructor(public navCtrl: NavController, public carritoSvc: CarritoService, public userSvc: UsernameService,

              private http:Http, private angularFire: AngularFireDatabase,
              public navParams: NavParams) {




              //this.cargar_compras();
  }

  public cargar_compras(){

    this.cargando = true;

    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/compras.json')
        .subscribe( res => {

           console.log( res.json() );

          // setTimeout( ()=>{
          this.cargando = false;
          this.compras = res.json();
          resolve();
          // },1500 )
          console.log(res.json());
        });

    });

    return promesa;

  }



  guardarCompra(){
    this.carritoSvc.guardarCompraBackend();
    this.carritoSvc.borrarCarrito();
    alert("Compra Realizada! Gracias por su Preferencia :)");
    this.navCtrl.setRoot(ProductosPage);
  }

  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }

}

