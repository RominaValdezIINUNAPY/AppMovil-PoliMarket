import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductosPage } from './productos';

//import {Ionic2Rating} from "./ionic2-rating";
//import {Ionic2RatingModule} from "ionic2-rating";

@NgModule({
  declarations: [
    ProductosPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductosPage),
    //Ionic2RatingModule
  ],
})
export class ProductosPageModule {}
