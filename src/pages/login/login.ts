import { Component , ViewChild} from '@angular/core';
import { NavController, IonicPage, NavParams} from 'ionic-angular';
import {ProductosPage} from "../productos/productos";
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import {ProductosService} from "../productos/productos.service";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import {UsernameService} from "../../services/user.service";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  facebook = {
    loggedIn : false,
    name : '',
    email : '',
    profilePicture: ''
  };

  public userId: string;
  _firstName: String;
  _gender: String;
  _lastName: String;
  _name: String;
  _data:any;

  isLoggedIn:boolean = false;
  users: any;

  @ViewChild('player') player;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private afauth:AngularFireAuth,
              public fb: Facebook,
              public nombreSvc: UsernameService) {

    fb.getLoginStatus()
      .then(res => {
        console.log(res.status);
        if(res.status === "connect") {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));


  }

  login() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if(res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  logout() {
    this.fb.logout()
      .then( res => this.isLoggedIn = false)
      .catch(e => console.log('Error logout from Facebook', e));
  }

  getUserDetail(userid) {
    this.fb.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        console.log(res);
        this.users = res;
      })
      .catch(e => {
        console.log(e);
      });
  }

  login2() {

    this.fb.login(['public_profile', 'user_friends', 'email','user_posts'])
      .then((res: FacebookLoginResponse) => {
        this.userId = res.authResponse.userID
        this.getUserInformation();
        // this.getUserFeeds();
        /*this.navCtrl.push('ProductosPage', {
          firstName: this._firstName,
          lastName: this._lastName,
          name: this._name,
          gender: this._gender,
          data:this._data
        //})*/
        //this.navCtrl.setRoot(ProductosPage);
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }
  getUserInformation() {
    this.fb.getLoginStatus().then((response) => {
      if (response.status == 'connected') {
        this.fb.api('/' + response.authResponse.userID + '?fields=id,name,gender,first_name,last_name', []).then((response) => {
          this._name = JSON.parse(JSON.stringify(response)).name;
          this._gender = JSON.parse(JSON.stringify(response)).gender;
          this._firstName = JSON.parse(JSON.stringify(response)).first_name;
          this._lastName = JSON.parse(JSON.stringify(response)).last_name;

          this.getUserFeeds();
        }, (error) => {
          alert(error);
        })
      }
    })
  }
  getUserFeeds() {
    this.fb.getLoginStatus().then((res) => {
      if (res.status == 'connected') {
        this.fb.api('/me/feed', []).then((res) => {
          this._data = res.data;
          // alert(JSON.stringify(this._data));
        }, (error) => {
          alert(error);
        })
      }
    })
  }

  ionViewWillLeave() {
    // the .nativeElement property of the ViewChild is the reference to the tag <video>
    this.player.nativeElement.src = '';
    this.player.nativeElement.load();
  }
  ionViewWillEnter() {
    this.player.nativeElement.src = 'assets/video/market_online.mp4';
    this.player.nativeElement.load();
  }

  ionViewDidLoad() {
    console.log('Hello LoginBackgroundVideo Page');
  }

  goToSignup() {
    console.log('Signup clicked');
  }

  goToLogin() {
    console.log('Login clicked');
  }


  loginwithfb() {
    this.afauth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => {
        this.facebook.loggedIn = true;
        this.getUserInformation();
        this.facebook.email = res.user.email;
        this.facebook.name = res.user.displayName;
        this.facebook.profilePicture = res.user.photoURL;
        this.nombreSvc.usuario = res.user.displayName;
        this.navCtrl.setRoot(ProductosPage);
      })
      //this.navCtrl.push('ProductosPage', {
      //  username: this.facebook.name

    //})
   // this.username.definirUsuario(this.user.facebook.name);
  }

  logoutwithfb(){
    this.facebook.loggedIn = false;
    this.afauth.auth.signOut();
  }

  goToProductos(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ProductosPage);
  }

}
