import { Injectable } from '@angular/core';
import {Producto} from "../models/producto";
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabase} from 'angularfire2/database';
import { Firebase } from '@ionic-native/firebase';
import {UsernameService} from "./user.service";


@Injectable()
export class CarritoService {
  productos:Producto[] = [];
  items:number = 0;
  total:number = 0;
  hayProductos: boolean = false;
  NuevoProducto: Producto;
  constructor( private angularFire: AngularFireDatabase, public nombreSvc: UsernameService) {

  }

  public agregarProducto(prod:Producto, cant:number){
    prod.cantidad = cant;
    this.productos.push(prod);
    this.items += cant;
    this.total += prod.precio * cant;
    this.hayProductos = true;
  }

  public agregarProducto3(nombre:string,categoria:string,codigo:string,url:string,precio:number,cant:number,comentario:string){
    this.NuevoProducto=new Producto();
    this.NuevoProducto.nombre=nombre;
    this.NuevoProducto.categoria=categoria;
    this.NuevoProducto.codigo=codigo;
    this.NuevoProducto.foto=url;
    this.NuevoProducto.cantidad=cant;
    this.NuevoProducto.precio=precio;
    this.NuevoProducto.comentario=comentario;
    this.productos.push(this.NuevoProducto);
    this.items += cant;
    this.total += precio * cant;
    this.hayProductos = true;
  }

  guardarCompraBackend() {

    for(var i = 0;i<this.productos.length;i++) {
      this.angularFire.list('compras').push(
        {
          usuario:this.nombreSvc.usuario,
          titulo:this.productos[i].nombre,
          categoria:this.productos[i].categoria,
          codigo:this.productos[i].codigo,
          url:this.productos[i].foto,
          cantidad:this.productos[i].cantidad,
          precio_unitario:this.productos[i].precio,
          precio_total:this.productos[i].precio*this.productos[i].cantidad,
          comentario:this.productos[i].comentario

        }

      ).then(
        (t: any) =>  {
          console.log('Compra guardada :)!: ' + t.key);

        },
        (e: any) => console.log(e.message));

      console.log(this.productos[i])
    }

  }

  public borrarCarrito(){
    this.productos = [];
    this.items = 0;
    this.total = 0;
    this.hayProductos = false;
  }

}
