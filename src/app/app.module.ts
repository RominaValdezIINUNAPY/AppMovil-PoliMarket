import {NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ProductosPage } from '../pages/productos/productos';
import { MiCarritoPage } from '../pages/mi-carrito/mi-carrito';
import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {CarritoService} from "../services/carrito.service";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Ionic2RatingModule } from 'ionic2-rating';
import {HttpModule} from "@angular/http";
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {ProductosService} from "../pages/productos/productos.service";
import {AngularFireDatabase} from "angularfire2/database";
import {UsernameService} from "../services/user.service";
import {Ionic2Rating} from "ionic2-rating";
import {Producto} from "../models/producto";
import {CurrentProductService} from "../services/current-product.service";

const firebase = {
  apiKey: "AIzaSyBNXmj-erMbafeWWjFu1m2mGtoBGajizxA",
  authDomain: "appmovil-6a513.firebaseapp.com",
  databaseURL: "https://appmovil-6a513.firebaseio.com",
  projectId: "appmovil-6a513",
  storageBucket: "appmovil-6a513.appspot.com",
  messagingSenderId: "809086394499"
};


@NgModule({
  declarations: [
    MyApp,
    ProductosPage,
    MiCarritoPage,
    MiPerfilPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(firebase),
    AngularFireAuthModule,
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProductosPage,
    MiCarritoPage,
    MiPerfilPage,
    LoginPage
    //Ionic2Rating
  ],
  providers: [
    CarritoService,
    UsernameService,
    CurrentProductService,
    Producto,
    LoginPage,
    StatusBar,
    Ionic2Rating,
    Facebook,
    AngularFireDatabase,
    ProductosService,
    SplashScreen,
    {provide:  ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
//export class Ionic2RatingModule {}


