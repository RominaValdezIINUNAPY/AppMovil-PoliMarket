import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CarritoService} from "../../services/carrito.service";
import {NgForm} from "@angular/forms";
import {ProductosPage} from "../productos/productos";
import {MiCarritoPage} from "../mi-carrito/mi-carrito";
import {UsernameService} from "../../services/user.service";
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'page-mi-perfil',
  templateUrl: 'mi-perfil.html'
})
export class MiPerfilPage {
  telefono:any;
  nacimiento:any;
  sexo:any;
  direccion:any;

  constructor(public navCtrl: NavController, private angularFire: AngularFireDatabase, public carritoSvc: CarritoService, public userSvc: UsernameService) {
    this.telefono=userSvc.telefono;
    this.nacimiento= this.userSvc.nacimiento;
    this.direccion=this.userSvc.direccion;
    this.sexo=this.userSvc.sexo;
  }

  guardarPerfil(f: NgForm){
    this.userSvc.telefono=f.controls.telefono.value;
    this.userSvc.nacimiento=f.controls.nacimiento.value;
    this.userSvc.sexo= f.controls.sexo.value;
    this.userSvc.direccion=f.controls.direccion.value;

    this.angularFire.list('usuarios').push(
      {
          nombre: this.userSvc.usuario,
          telefono:f.controls.telefono.value,
          nacimiento:f.controls.nacimiento.value,
          sexo: f.controls.sexo.value,
          direccion:f.controls.direccion.value

        }

      ).then(
        (t: any) =>  {
          console.log('Datos guardados!: ' + t.key);
          alert("Datos Guardados! :) ");

        },
        (e: any) => console.log(e.message));

    }



  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }
}
