import { HintModalPage } from './hint-modal';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import {Ionic2Rating} from "ionic2-rating";

@NgModule({
  declarations: [
    HintModalPage,
  ],
  imports: [
    IonicPageModule.forChild(HintModalPage),
    Ionic2RatingModule
  ],
  exports: [
    HintModalPage
  ],
  providers: [
    Ionic2Rating
  ]
})

export class HintModalPageModule { }
