import { Injectable } from '@angular/core';
import {LoginPage} from "../login/login";
import {Producto} from "../../models/producto";
import {Http} from "@angular/http";


@Injectable()
export class ProductosService {
  //productos:Producto[] = [];
  items:number = 0;
  total:number = 0;
  hayProductos: boolean = false;
  producto_array:any[] = [];
  productos:any[] = [];
  productos_detalles:any[] = [];
  productos_filtrado:any[] = [];
  cargando:boolean = true;

  constructor( private http:Http ) {

    this.cargar_productos();

  }

  public buscar_producto( termino:string ){

    // console.log("Buscando producto");
    // console.log( this.productos.length );


    if( this.productos.length === 0 ){
      this.cargar_productos().then( ()=>{
        // termino la carga
        this.filtrar_productos(termino);
      });
    }else{
      this.filtrar_productos(termino);
    }

  }

  private filtrar_productos(termino:string){

    this.productos_filtrado = [];

    termino = termino.toLowerCase();

    this.productos.forEach( prod =>{

      if( prod.categoria.indexOf( termino ) >=0 || prod.titulo.toLowerCase().indexOf( termino ) >=0 ){
        this.productos_filtrado.push( prod );
        // console.log( prod );
      }

      // console.log( prod );

    })

  }




  public cargar_productos(){

    this.cargando = true;

    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/Productos_idx.json')
        .subscribe( res => {

          // console.log( res.json() );

          // setTimeout( ()=>{
          this.cargando = false;
          this.productos = res.json();
          resolve();
          // },1500 )
        });

    });

    return promesa;



  }



  definirUsuario(username:LoginPage){


    //return this.username.facebook.name;
  }



  public agregarProducto(prod:Producto, cant:number){
    prod.cantidad = cant;
    this.productos.push(prod);
    this.items += cant;
    this.total += prod.precio * cant;
    this.hayProductos = true;
  }

  public borrarCarrito(){
    this.productos = [];
    this.items = 0;
    this.total = 0;
    this.hayProductos = false;
  }

  public cargar_producto( cod:string ){

    return this.http.get(`https://webangular4.firebaseio.com/Productos/${ cod }.json`);

  }

  public cargar_producto2(cod:string){

    this.cargando = true;

    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get(`https://webangular4.firebaseio.com/Productos/${ cod }.json`)
        .subscribe( res => {

          // console.log( res.json() );

          // setTimeout( ()=>{
          this.cargando = false;
          this.productos_detalles = res.json();

          resolve();
          // },1500 )
        });

    });

    return promesa;

  }




}
