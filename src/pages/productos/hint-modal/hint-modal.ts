import { Component } from '@angular/core';
import {NavParams, ViewController, IonicPage, ModalController} from 'ionic-angular';
import {AlertController} from "ionic-angular";
import {Ionic2Rating} from "ionic2-rating";
import {ProductosPage} from "../productos";
import {ProductosService} from "../productos.service";
import {Producto} from "../../../models/producto";
import {CurrentProductService} from "../../../services/current-product.service";


@IonicPage()
@Component({
  selector: 'page-hint-modal',
  templateUrl: 'hint-modal.html'
})
export class HintModalPage {

  myParam: string;

  constructor(
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    params: NavParams,public alertCtrl: AlertController, public rating:Ionic2Rating,
    private ps:ProductosService, public producto:Producto,
    public currentProd:CurrentProductService
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onModelChange(e){
    console.log('Calificación Estrellas: '+e);
    //onModelChange();
    //this.ps.productos_detalles;

  }




}
