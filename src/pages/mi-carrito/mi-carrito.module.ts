import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiCarritoPage } from './mi-carrito';

@NgModule({
  declarations: [
    MiCarritoPage,
  ],
  imports: [
    IonicPageModule.forChild(MiCarritoPage),
  ],
})
export class MiCarritoPageModule {}
