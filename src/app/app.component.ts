import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { ProductosPage } from '../pages/productos/productos';
import {MiCarritoPage} from "../pages/mi-carrito/mi-carrito";
import {LoginPage} from "../pages/login/login";



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  goToMiPerfil(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiPerfilPage);
  }
  goToProductos(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ProductosPage);
  }
  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }
  goToLogin(params){
    if (!params) params = {};
    this.navCtrl.setRoot(LoginPage);
  }
}

