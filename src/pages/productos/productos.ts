import {Component, Injectable,OnInit,forwardRef, Input} from '@angular/core';
import { NavController , NavParams, IonicPage, ModalController} from 'ionic-angular';
import { MiCarritoPage } from '../mi-carrito/mi-carrito';
import {CarritoService} from "../../services/carrito.service";
import {Producto} from "../../models/producto";
import {LoginPage} from "../login/login";
import {ProductosService} from "./productos.service";
import {Http} from "@angular/http";

import {AngularFireDatabase} from 'angularfire2/database';
import { NgForm } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import {UsernameService} from "../../services/user.service";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import {Ionic2Rating} from "ionic2-rating";
import {CurrentProductService} from "../../services/current-product.service";



@IonicPage()
@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
})

//@Injectable
export class ProductosPage implements OnInit{
  rootPage: any;
  ngOnInit() {
  }

  //usuario= this.printUsername();
  user:string='';
  //usuario=this.definirUsuario();

  //prod = new Producto();
  //productos: Producto[] = [this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod,this.prod];

  firstName:string;
  gender:string;
  lastName:string;
  data:any;
  //productos:Producto[] = [];
  items:number = 0;
  total:number = 0;
  hayProductos: boolean = false;
  compras:any[] = [];
  productos:any[] = [];
  productos_filtrado:any[] = [];
  cargando:boolean = true;
  cantidad: number=0;
  comenta: string;
  aux_coment:any[]=[];


  constructor(private angularFire: AngularFireDatabase, public navCtrl: NavController, public carritoSvc: CarritoService,
              public ps: ProductosService, public navParams: NavParams, private http:Http,
              public alertCtrl: AlertController, public userSvc: UsernameService,
              public modalCtrl: ModalController, public  producto:Producto, public  currentProduct:CurrentProductService){

              //this.firstName = navParams.get("username");


              this.cargar_productos();
  }

  form_submit(f: NgForm) {


    this.angularFire.list('compras').push(
      {
        titulo:f.controls.titulo.value,
        categoria:f.controls.categoria.value,
        codigo: f.controls.codigo.value,
        url:f.controls.url.value

      }

    ).then(
      (t: any) =>  {
        console.log('Datos guardados!: ' + t.key);

      },
      (e: any) => console.log(e.message));

  }


  public buscar_producto( termino:string ){

    // console.log("Buscando producto");
    // console.log( this.productos.length );


    if( this.productos.length === 0 ){
      this.cargar_productos().then( ()=>{
        // termino la carga
        this.filtrar_productos(termino);
      });
    }else{
      this.filtrar_productos(termino);
    }

  }

  private filtrar_productos(termino:string){

    this.productos_filtrado = [];

    termino = termino.toLowerCase();

    this.productos.forEach( prod =>{

      if( prod.categoria.indexOf( termino ) >=0 || prod.titulo.toLowerCase().indexOf( termino ) >=0 ){
        this.productos_filtrado.push( prod );
        // console.log( prod );
      }

      // console.log( prod );

    })

  }


  public cargar_producto( cod:string ){

    return this.http.get(`https://appmovil-6a513.firebaseio.com/Productos/${ cod }.json`);

  }


  public cargar_productos(){

    this.cargando = true;

    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/Productos_idx.json')
        .subscribe( res => {

          // console.log( res.json() );

          // setTimeout( ()=>{
          this.cargando = false;
          this.productos = res.json();

          resolve();
          // },1500 )
        });

    });

    return promesa;

  }


  //user=this.firstName;

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductosPage');
  }

  printUsername(){
  return this.firstName;
  }



  agregarAMiCarrito(prod:Producto){
    this.carritoSvc.agregarProducto(prod,5);
  }

  agregarAMiCarrito2(nombre:string,categoria:string,codigo:string,url:string){
   console.log('producto seleccionado: '+nombre+','+categoria+','+codigo+','+url);

    this.angularFire.list('compras').push(
      {
        titulo:nombre,
        categoria:categoria,
        codigo: codigo,
        url:url

      }

    ).then(
      (t: any) =>  {
        console.log('Datos guardados!: ' + t.key);

      },
      (e: any) => console.log(e.message));

  }

  agregarAMiCarrito3(nombre:string,categoria:string,codigo:string,url:string,precio:number,cant:number,comentario:string){
    console.log('cantridad compra: '+cant);
    console.log('comentario: '+comentario);
    this.carritoSvc.agregarProducto3(nombre,categoria,codigo,url,precio,cant,comentario);
  }

  agregarAMiCarritoCantidad(cant:number){
    return cant;
  }

  goToMiCarrito(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MiCarritoPage);
  }

  presentAlert(titulo:string,categoria:string,cod:string,url:string,precio:number) {
    var texto: string;
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: categoria,


      buttons: ['Ok']
    });
    alert.present();
  }

  presentConfirm(titulo:string,categoria:string,cod:string,url:string,precio:number) {
    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/compras.json')
        .subscribe( res => {

          console.log( res.json() );

          // setTimeout( ()=>{
          //this.cargando = false;
          this.compras = res.json();
          resolve();
          // },1500 )
          console.log(res.json());

          console.log( 'cod de click: '+cod);
          //console.log( 'codigo de compra: '+this.compras[0].codigo);
          console.log(this.compras);
          for (let compra of this.compras) {
            console.log( 'codigo de compra: '+compra.codigo);
            if (compra.codigo==cod){
              console.log('Compra Codigo: '+compra.codigo+' cod de click: '+cod);
              this.aux_coment=compra.comentario;
            }
          }

          var comentario_conc:string='';

          for (let i in this.aux_coment) {
            comentario_conc += this.aux_coment[i];
          }

              const alert = this.alertCtrl.create({
                title: 'Producto: '+titulo +' '+'Categoría:  '+ categoria +'  '+'Código: '+ cod +'  '+'Precio: '+ precio,

                message: comentario_conc,
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                      //console.log('Cancel clicked');
                    }
                  }

                ]
              });
              alert.present();




        });

    });

  }

  presentPrompt(nombre:string,categoria:string,codigo:string,url:string,precio:number) {
    const alert = this.alertCtrl.create({
      title: 'Producto',
      inputs: [
        {
          name: 'cantidad',
          placeholder: 'cantidad'
        },
        {
          name: 'comentario',
          placeholder: 'comentario',

        }


      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: data => {
          this.agregarAMiCarrito3(nombre,categoria,codigo,url,
            precio,data.cantidad,data.comentario);



          }
        }
      ]
    });
    alert.present();

     setTimeout( ()=>{
        this.openHintModal();
     },6000 )
  }
  onModelChange(e){

    console.log('Calificación Estrellas: '+e);
    //onModelChange();
    return e;
  }

  openHintModal() {
    this.openModal('HintModalPage');
  }

  /*openModal(pageName, url:string) {
    this.modalCtrl.create(pageName, null, { cssClass: 'inset-modal' },url);
      .present();
  }*/

  openModal(pageName) {
    this.modalCtrl.create(pageName, null, { cssClass: 'inset-modal' })
  .present();
  }

  public cargar_compras(){

    //this.cargando = true;

    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/compras.json')
        .subscribe( res => {

          console.log( res.json() );

          // setTimeout( ()=>{
          //this.cargando = false;
          this.compras = res.json();
          resolve();
          // },1500 )
          console.log(res.json());


        });

    });

    return promesa;

  }

  verDetalles(prod:any, cod:any){
    this.currentProduct.producto= prod;




    let promesa = new Promise( ( resolve, reject )=>{

      this.http.get('https://appmovil-6a513.firebaseio.com/compras.json')
        .subscribe( res => {

          console.log( res.json() );

          // setTimeout( ()=>{
          //this.cargando = false;
          this.compras = res.json();
          resolve();
          // },1500 )
          console.log(res.json());

          console.log( 'cod de click: '+cod);
          //console.log( 'codigo de compra: '+this.compras[0].codigo);
          console.log(this.compras);
          for (let compra of this.compras) {
            console.log( 'codigo de compra: '+compra.codigo);
            if (compra.codigo==cod){
              console.log('Compra Codigo: '+compra.codigo+' cod de click: '+cod);
              this.aux_coment=compra.comentario;
            }
          }

          var comentario_conc:string='';

          for (let i in this.aux_coment) {
            comentario_conc = comentario_conc + this.aux_coment[i];
          }


          this.currentProduct.comentario = comentario_conc;

          console.log('Comentarios = ' + comentario_conc);



        });

    });


    this.modalCtrl.create('HintModalPage2', null, { cssClass: 'inset-modal' })
      .present();
    //this.openHintModal();

  }


}




