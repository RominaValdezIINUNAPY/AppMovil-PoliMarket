import { HintModalPage2 } from './hint-modal2';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import {Ionic2Rating} from "ionic2-rating";

@NgModule({
  declarations: [
    HintModalPage2,
  ],
  imports: [
    IonicPageModule.forChild(HintModalPage2),
    Ionic2RatingModule
  ],
  exports: [
    HintModalPage2
  ],
  providers: [

  ]
})

export class HintModalPage2Module { }
